import typer
import os

app = typer.Typer()

file_name = "test.txt"


@app.command()
def c(file_name: str):
    file_stat = os.stat(file_name)
    #print(file_stat.st_size)
    typer.secho(file_stat.st_size)

@app.command()
def l(file_name: str):
    with open(file_name,'r') as fp:
        lines = len(fp.readlines())
    typer.secho(f"The number of lines is: {lines}")

@app.command()
def w(file_name: str):
    with open(file_name,'r') as file:
        data = file.read()
        lines = data.split()
        number_of_words = 0
        number_of_words += len(lines)
    typer.secho(f"The number of words is: {number_of_words}")

@app.command()
def m(file_name: str):
    with open(file_name,'r') as file:
        content = file.read()
        num_characters = len(content)
    
    typer.secho(f"The number of characters is: {num_characters}")

@app.command()
def app():
    #typer.secho("HEllo World")
    with open(file_name,'r') as file:
        file_size = c(file_name)
        num_ofLines = l(file_name)
        number_ofWords = w(file_name)
        typer.secho(f"{file_size} {num_ofLines} {number_ofWords}")
        return

if __name__ == "__main__":
    #typer.run(c)
    
    app()
